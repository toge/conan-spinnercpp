from conans import ConanFile

class SpinnercpConan(ConanFile):
    name           = "spinnercpp"
    version        = "20191008"
    license        = "MIT"
    author         = "toge.gmail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-spinnercpp/"
    homepage       = "https://github.com/trumae/spinnercpp"
    description    = "Simple header only library to add a spinner / progress indicator to any terminal application."
    topics         = ("terminal", "progress", "spinner")
    generators     = "cmake"
    no_copy_source = True
    settings       = "os"

    def source(self):
        self.run("git clone https://github.com/trumae/spinnercpp")
        self.run("cd spinnercpp && git checkout eaeeb4ef88aa3095ac4560a72cff6ea04630f624")

    def package(self):
        self.copy("spinnercpp.h", dst="include", src="spinnercpp")

    def package_info(self):
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["pthread"]

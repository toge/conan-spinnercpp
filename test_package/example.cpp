#include <memory>

#include "spinnercpp.h"

int main() {
    auto spin = std::make_unique<spinnercpp::spinner>();
    spin->start();
}
